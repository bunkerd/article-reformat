/* global userDateRestring dateStr */
'use strict';

$( document ).ready( () => {
	
	/** Updates date previews and
	 *  whether or not the modification date is visible. */
	const updateDates = () => {
		let dateGiven = false;
		const lang = $( '#lang' ).text();
		for ( const key of [ 'pub', 'mod' ] ) {
			const d = dateStr( $( `#${ key }date` ).val(), lang );
			if ( d ) dateGiven = true;
			if ( d === null ) {
				$( `#${ key }datetxt` ).addClass( 'invalid' );
				$( `#${ key }datetxt` ).text( 'invalid' );
			} else {
				$( `#${ key }datetxt` ).removeClass( 'invalid' );
				$( `#${ key }datetxt` ).text( d || '(YYYY-MM-DD hh:mm)' );
			}
		}
		if ( dateGiven ) $( '#moddateblock' ).removeClass( 'hidden' );
		else $( '#moddateblock' ).addClass( 'hidden' );
	};

	/** Fills the input fields with pre-existing values
	 *  @param {Object} meta - Contains the values to set  */
	const initInputs = ( meta ) => {
		for ( const key of [ 'publisher', 'title', 'author', 'pubdate', 'moddate' ] ) {
			$( '#' + key ).val( meta[ key ] || '' );
		}
		if ( meta.lang === 'fr' ) $( '#lang' ).text( 'fr' );
	};

	// Initialize page reading if needed, get the metadata
	chrome.tabs.query( { active: true, currentWindow: true }, function( tabs ) {
		chrome.tabs.sendMessage( tabs[ 0 ].id, { bdreformat_MSG: true }, function( meta ) {

			// Initialize text values
			initInputs( meta );

			// Input events
			for ( const key of [ 'publisher', 'title', 'author', 'pubdate', 'moddate' ] ) {
				if ( key.substr( 3 ) === 'date' ) {
					$( '#' + key ).change( function() {
						const v = userDateRestring( $( this ).val() );
						if ( dateStr( v ) === null ) {
							$( this ).val( meta[ key ] );
							updateDates();
						} else {
							$( this ).val( v );
							meta[ key ] = v;
							chrome.tabs.sendMessage( tabs[ 0 ].id, { bdreformat_MSG: 'setmeta', [ key ]: v } );
						}
					} );
				} else {
					$( '#' + key ).change( function() {
						chrome.tabs.sendMessage( tabs[ 0 ].id, { bdreformat_MSG: 'setmeta', [ key ]: $( this ).val() } );
					} );
				}
			}
			$( '#lang' ).click( function() {
				const newlang = ( $( this ).text() === 'fr' ) ? 'en' : 'fr';
				$( this ).text( newlang );
				chrome.tabs.sendMessage( tabs[ 0 ].id, { bdreformat_MSG: 'setmeta', lang: newlang } );
				updateDates();
			} );
			for ( const key of [ '#pubdate', '#moddate' ] ) {
				$( key ).on( 'input', function() {
					$( this ).val( userDateRestring( $( this ).val(), true ) );
					updateDates();
				} );
			}
			updateDates();

			// Bottom buttons
			$( '#rewrite' ).click( function() { // Rewrite the article
				chrome.tabs.query( { active: true, currentWindow: true }, function( t ) {
					chrome.tabs.sendMessage( t[ 0 ].id, { bdreformat_MSG: 'rewrite' } );
				} );
				window.close();
			} );
			$( '#cancel' ).click( function() { // Cancel the use of the plugin
				chrome.tabs.query( { active: true, currentWindow: true }, function( t ) {
					chrome.tabs.sendMessage( t[ 0 ].id, { bdreformat_MSG: 'clean' } );
				} );
				window.close();
			} );
			$( '#info' ).hover( // Manual
				() => $( '#infobox' ).removeClass( 'hidden' ),
				() => $( '#infobox' ).addClass( 'hidden' )
			);
			const reset = ( full ) => {
				const req = ( full ) ? 'fullreset' : 'reset';
				chrome.tabs.query( { active: true, currentWindow: true }, function( t ) {
					chrome.tabs.sendMessage( t[ 0 ].id, { bdreformat_MSG: req }, function( rmeta ) {
						initInputs( rmeta );
					} );
				} );
			};
			if ( meta.known ) {
				$( '#reset' ).click( () => $( '#resetbox' ).removeClass( 'hidden' ) );
				$( '#fullreset' ).click( function() {
					reset( true );
					$( '#resetbox' ).addClass( 'hidden' );
				} );
				$( '#pagereset' ).click( function() {
					reset();
					$( '#resetbox' ).addClass( 'hidden' );
				} );
			} else {
				$( '#reset' ).click( () => reset() );
			}

		} );
	} );

} );

/**
 * chrome.storage.sync.set    ( { key: value } , function() { ... } )
 * chrome.storage.sync.get    ( { key: value } , function() { ... } )
 * chrome.storage.sync.remove (   key          , function() { ... } )
 * chrome.storage.sync.clean  (                  function() { ... } )
 */

/**
 * Learn:
 *     publisher:
 *         Store
 *     title, descr, paragraph, section:
 *         For all elements of a role*:
 *             Get the selector (up to body)
 *             Get all the matching elements in the page
 *             If the majority indeed has the role, store the selector
 *             * In order to go faster, use a while on the list of elements,
 *               and remove all elements from the list having the same descriptor at each loop.
 *               /!\ If selector is selected, can remove all matching elements
 *                   Else, should keep element with classes not covered by the selector
 *     author, date:
 *         Try to check if there is some way to find in the html:
 *         author as a text content, date as a datetime attribute.
 * 
 * Apply knowledge:
 *     If any...
 *     For each selector, apply corresponding role.
 *     (For date and authors, use a new class that shall be deleted when field editied.)
 */
