/* exported metaDateRestring userDateRestring earliest latest dateStr dateCompact */
// Use the standardized string format: ‘YYYY-MM-DD hh:mm’, stopping at specified level

/** Converts a date string written by the user from html meta into the local standard format.
 *  @param {string} d - Date string
 *  @param {boolean} [writing] - True if the user is currently typing (relaxed reformatting)
 *  @returns {string} Same date in the local standard format
 */
const userDateRestring = ( d, writing ) => {
	let date = '';
	let sep = [ ':', ' ', '-', '-' ];
	while ( d ) {
		d = d.replace( /([^0-9]*)([0-9]*)/, ( _, s, n ) => {
			if ( ! n ) {
				if ( s && date && writing ) date += sep.pop();
				return '';
			}
			if ( ! date ) {
				date = n.substr( 0, 4 );
				return n.substr( 4 );
			}
			if ( writing ) {
				date += sep.pop() + n.substr( 0, 2 );
			} else {
				if ( ! Number( n ) ) {
					sep = [];
					return '';
				}
				date += sep.pop() + ( ( n.length === 1 ) ? '0' : '' ) + n.substr( 0, 2 );
			}
			return n.substr( 2 );
		} );
		if ( ! sep.length ) break;
	}
	return date;
};

/** Sets a number in a string format with a given number of digits
 *  @param {(string|number)} n - Number to write
 *  @param {number} digits - Required number of digits
 *  @returns {string} - Formatted number string
 */
const digExpand = ( n, digits ) => {
	n = '' + n;
	while ( n.length < digits ) n = '0' + n;
	return n;
};

/** Converts a date string from html meta into the local standard format.
 *  If daytime is given but is 00:00, it is considered as not given.
 *  @param {string} d - Date string found in the html as meta.
 *  @returns {string} Same date in the local standard format
 */
const metaDateRestring = ( d ) => {
	if ( ! d.match( /[0-9]/ ) ) return '';
	const date = new Date( d );
	// Identify the accuracy
	let hasMonth, hasDay;
	const hasTime = date.getUTCHours() || date.getUTCMinutes() || date.getUTCSeconds() || date.getUTCMilliseconds();
	if ( hasTime ) {
		hasMonth = true;
		hasDay = true;
	} else {
		d = d.split( /[^0-9]+/ );
		if ( ! d[ 0 ] ) d.shift();
		if ( date.getUTCDate() > 1 || d[ 2 ] ) {
			hasDay = true;
			hasMonth = true;
		} else {
			hasMonth = date.getUTCMonth() || d[ 1 ];
		}
	}
	// Rewrite the date
	d = digExpand( date.getUTCFullYear(), 4 );
	if ( hasMonth ) {
		d += '-' + digExpand( date.getUTCMonth() + 1, 2 );
		if ( hasDay ) {
			d += '-' + digExpand( date.getUTCDate(), 2 );
			if ( hasTime ) {
				d += ' ' + digExpand( date.getUTCHours(), 2 ) + ':' + digExpand( date.getUTCMinutes(), 2 );
			}
		}
	}
	return d;
};

/** Identify the earliest date among two dates in local standard string format, taking into accountz accuracy variations.
 *  @param {string} a - Date
 *  @param {string} b - Date
 *  @return {string} - Latest date
 */
const earliest = ( a, b ) => {
	if ( a.length === b.length ) return ( ( a < b ) ? a : b );
	const n = Math.min( a.length, b.length );
	const ax = a.substr( 0, n );
	const bx = b.substr( 0, n );
	if ( ax === bx ) return ( a.length > b.length ) ? a : b;
	return ( ax < bx ) ? a : b;
};

/** Identify the latest date among two dates in local standard string format, taking into accountz accuracy variations.
 *  @param {string} a - Date
 *  @param {string} b - Date
 *  @return {string} - Latest date
 */
const latest = ( a, b ) => (
	( a > b ) ? a : b
);

/** Converts a date to a word-based format (with short months), in the specified language.
 *  @param {Date} d - Date to write
 *  @param {string} [lang] - Language: 'fr' or 'en' (default)
 *  @returns {string} Formatted date ('' if empty, null if invalid)
 */
const dateStr = ( d, lang ) => {
	d = d.split( /[^0-9]/ );
	if ( ! d[ 0 ] ) return '';
	if ( ! d[ 1 ] ) return d[ 0 ];
	// Convert data beyond year into numbers, check that the date is valid
	for ( let k = 1; k < d.length; k++ ) {
		d[ k ] = Number( d[ k ] );
		if ( d[ k ] < [ 0, 1, 1, 0, 0 ][ k ] ) return null;
		if ( d[ k ] > [ 0, 12, 31, 23, 59 ][ k ] ) return null;
		if ( k === 2 ) {
			if ( ( d[ 2 ] === 31 ) && [ 4, 6, 9, 11 ].includes( d[ 1 ] ) ) return null;
			if ( d[ 1 ] === 2 ) {
				if ( d[ 2 ] > 29 ) return null;
				if ( ( d[ 2 ] === 29 ) && ( ( d[ 0 ] % 4 ) || ( ( d[ 0 ] % 400 ) && ! ( d[ 0 ] % 100 ) ) ) ) return null;
			}
		}
	}
	// Convert into string
	if ( lang === 'fr' ) {
		// Date in French
		let s = [ 'Jan.', 'Fév.', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Ao' + String.fromCharCode( 251 ) + 't', 'Sept.', 'Oct.', 'Nov.', 'Déc.' ][ d[ 1 ] - 1 ] + ' ' + d[ 0 ];
		if ( d[ 2 ] ) {
			s = d[ 2 ] + ' ' + s;
			if ( d.length > 3 ) s += ', ' + digExpand( d[ 3 ], 2 ) + ':' + digExpand( d[ 4 ] || 0, 2 );
		}
		return s;
	}
	// Date in English
	let s =
		[ 'Jan.', 'Feb.', 'March', 'Apr.', 'May', 'June', 'Jul.', 'Aug.', 'Sept.', 'Oct.', 'Nov.', 'Dec.' ][ d[ 1 ] - 1 ] +
		( ( d[ 2 ] ) ? ' ' + d[ 2 ] + ', ' : ' ' ) +
		d[ 0 ];
	if ( d.length > 3 ) {
		s += digExpand( ( ( d[ 3 ] + 11 ) % 12 ) + 1, 2 ) + ':' +
			digExpand( d[ 4 ] || 0, 2 ) +
			( ( d[ 3 ] < 12 ) ? ' AM' : ' PM' );
	}
	return s;
};
