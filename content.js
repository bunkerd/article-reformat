/* global metaDateRestring earliest latest dateStr */
/** DOM object as jQuery
 *  @typedef {jQuery} $DOM */
'use strict';

( () => {
	
	/********************\
	|  USEFUL CONSTANTS  |
	\********************/
	
	const hostname = window.location.hostname;

	// DOM elements
	const $body = $( 'body' );
	const $head = $( 'head' );

	const formatTags = [
		'EM', 'I', 'B', 'STRONG', 'U', 'STRIKE', 'DEL', 'SPAN',
		'TT', 'A', 'MARK', 'SUP', 'SUB', 'SMALL', 'Q', 'CODE',
		'VAR', 'KBD', 'PRE', 'BR' ];

	// Unique id for the plugin, ensuring unique class names etc
	let snipid;
	while ( true ) {
		snipid = String.fromCharCode( ...Array.from( { length: 7 }, () => Math.floor( Math.random() * 26 ) + 97 ) );
		if ( $body.find( `[class*="${ snipid }-"]` ).length ) continue;
		if ( $body.find( `meta[data-${ snipid }-metastorage]` ).length ) continue;
		if ( $body.find( `style[data-${ snipid }]` ).length ) continue;
		if ( $body[ 0 ][ `${ snipid }-clean` ] ) continue;
		if ( $body[ 0 ][ `${ snipid }-initrules` ] ) continue;
		break;
	}

	// Define classes
	const CLASS = { title: '', descr: '', paragraph: '', section: '', mouseover: '', hidden: '' };
	for ( const key in CLASS ) {
		CLASS[ key ] = snipid + '-' + key;
	}

	// Get stored info about the domain
	chrome.storage.sync.get( [ hostname ], function( storedRules ) {
		if ( storedRules[ hostname ] ) $body[ 0 ][ `${ snipid }-initrules` ] = storedRules[ hostname ];
	} );

	/*******************\
	|  EXTRACTING TEXT  |
	\*******************/

	/** Recreate a DOM object from other DOM objects by only keeping formatted text, i.e. text nodes and
	 *  formatting elements (such as <em>, <strong>, etc.).
	 *  If a target DOM element is given as the ‘into’ argument, the formatted text is appended into it.
	 *  Else, ‘into’ can specify a tag name, else a paragraph is created.
	 *  @param {$DOM} el - Elements from which to extract the text
	 *  @param {($DOM|string)} [into] - Element to create or complete (as $DOM or tag string)
	 *  @returns {$DOM} The created or completed DOM element.
	 */
	const textify = ( el, into ) => {
		// Initialiaze the target element
		if ( ! into ) into = 'p';
		if ( typeof into === 'string' ) into = $( document.createElement( into ) );
		// Loop through nodes
		el.contents().each( function() {
			switch ( this.nodeType ) {
				case 1:
					// For elements, keep the tag if formatting, go deeper
					if ( formatTags.includes( this.tagName ) ) {
						const e = $( document.createElement( this.tagName ) );
						into.append( e );
						if ( this.tagName === 'A' ) {
							e.attr( 'href', this.getAttribute( 'href' ) );
							e.attr( 'target', '_blank' );
						}
						textify( $( this ), e );
					} else {
						textify( $( this ), into );
					}
					break;
				case 3:
					// For text, just keep
					const lc = into[ 0 ].lastChild;
					if ( lc && lc.nodeType === 3 ) {
						lc.textContent += this.textContent;
					} else {
						into.append( document.createTextNode( this.textContent ) );
					}
					break;
			}
		} );
		// Done
		return into;
	};

	/************\
	|  METADATA  |
	\************/

	/* Metadata will be used to store varous information:
		> .publisher :  Publisher / Journal name
		> .pubdate :    Publication date
		> .moddate :    Modification date
		> .author :     Author
		> .title :      Title of the article
		> .descr :      Description, according to the page metadata
		> .lang :       Language, as 'fr' if French
	*/

	/** Describes typical locations for meta information */
	const metaMap = {
		publisher:
			[ [
				'property="article:publisher"',
				'property="og:site_name"',
			], [
				'property="al:ios:app_name"',
				'name="twitter:app:name:googleplay"',
				'name="twitter:app:name:ipad"',
				'name="twitter:app:name:iphone"',
			] ],
		pubdate:
			[ [
				'name="pubdate"',
				'property="article:published_time"',
				'property="og:article:published_time"',
			], [
				'name="date"',
				'name="sailthru.date"',
			] ],
		moddate:
			[ [
				'name="lastmod"',
				'property="article:modified_time"',
				'property="og:article:modified_time"',
			], [
				'name="date"',
				'name="sailthru.date"',
			] ],
		author:
			[ [
				'property="author"',
				'property="article:author"',
				'property="og:article:author"',
				'name="sailthru.author"',
			] ],
		title:
			[ [
				'title',
				'name="title"',
				'property="og:title"',
				'property="twitter:title"',
				'name="sailthru.title"',
			] ],
		desrc:
			[ [
				'name="description"',
				'property="og:description"',
				'property="twitter:description"',
				'name="sailthru.description"',
			] ],
	};

	/** Stores/corrects informations as meta elements. (Does not delete omitted, pre-existing parameters.)
	 *  @param {object.<string, string>} meta - Metadata to store
	 */
	const setMeta = ( meta ) => {
		for ( const key in meta ) {
			$( `meta[data-${ snipid }-metastorage][property="${ key }"]` ).remove();
			if ( meta[ key ] ) {
				meta[ key ] = meta[ key ].trim();
				if ( meta[ key ] ) $head.append( `<meta data-${ snipid }-metastorage property="${ key }" content="${ meta[ key ] }">` );
			}
		}
	};

	/** Returns stored metadata.
	 *  @returns {object.<string, string>} Stored metadata
	 */
	const getMeta = () => {
		const meta = {};
		$( `meta[data-${ snipid }-metastorage]` ).each( function() {
			meta[ $( this ).attr( 'property' ) ] = $( this ).attr( 'content' );
		} );
		return meta;
	};

	/** Registers, through DOM, a string or $DOM element as being the title
	 *  @param {($DOM|string)} title - Title
	 *  @returns {string} Title string (null if no title)
	 */
	const setTitleEl = ( title ) => {
		// Current title
		const titleEl = $body.find( `.${ CLASS.title }` );
		const newTitle = ( ( typeof title === 'string' ) ? title : title.text() ).trim();
		// Do nothing if title is a string already matching the identified title
		if ( newTitle === titleEl.text().trim() ) return;
		// Clean the existing title
		titleEl.filter( `.${ CLASS.hidden }` ).remove();
		titleEl.removeClass( CLASS.title );
		// Set the title
		if ( newTitle ) {
			// If string input, create a DOM element
			if ( typeof title === 'string' ) {
				const t = $( document.createElement( 'div' ) );
				t.addClass( CLASS.title );
				t.addClass( CLASS.hidden );
				t.text( newTitle );
				$body.prepend( t );
				return newTitle;
			}
			// Else
			// Clean the roles of the new title
			for ( const c in CLASS ) {
				title.find( '.' + c ).removeClass( CLASS[ c ] );
				title.removeClass( CLASS[ c ] );
				title.parents().removeClass( CLASS[ c ] );
			}
			// Gives title role
			title.addClass( CLASS.title );
			return newTitle;
		}
		return null;
	};

	/*****************************\
	|  LEARNING FUNCTIONNALITIES  |
	\*****************************/

	/* Rules are store in a dictionnary, with fields:
	 *   .publisher, .lang:
	 *      > The value, as a string
	 *   .title, .descr, .paragraph, .section
	 *      > A selector to identify the element(s).
	 *   .author, .pubdate, .moddate:
	 *      > Either an array describing the relevant <meta> fields (similar to one level in metaMap),
	 *      > or a selector to identify the element(s).
	 */

	/** Update a set of rules for content identification on the basis of the current roles and meta data. */
	const updateLearned = () => {
		const rules = $body[ 0 ][ `${ snipid }-initrules` ] || {};
		const fullSelector = ( el ) => {
			const appPrefix = snipid + '-';
			let sel;
			if ( el.id ) {
				sel = el.tagName + '#' + el.id;
			} else {
				sel = ( el.className || '' ).trim();
				if ( sel ) {
					const classes = sel.split( /\s+/ ).sort();
					sel = el.tagName;
					for ( const c of classes ) {
						// Do not include classes added by the extension
						if ( ! c.startsWith( appPrefix ) ) sel += '.' + c;
					}
				} else {
					sel = el.tagName;
				}
			}
			if ( el.tagName === 'BODY' ) return sel;
			return fullSelector( el.parentNode ) + '>' + sel;
		};
		const meta = getMeta();
		// Publisher -----
		if ( meta.publisher ) rules.publisher = meta.publisher;
		else delete rules.publisher;
		// Language -----
		if ( meta.lang ) rules.lang = meta.lang;
		else delete rules.lang;
		// Title -----
		let el = $( '.' + CLASS.title );
		if ( el.length && ! el.hasClass( CLASS.hidden ) ) rules.title = fullSelector( el[ 0 ] );
		else delete rules.title;
		// Author -----
		//  Check the existence of a working rule
		//TODO Don't do that if initial meta reading was successful. Use another data- field to the html element.
		//TODO try getting the info in meta first
		if ( ! ( rules.author && $( rules.author ).text() === ( meta.author || '' ) ) ) {
			// If no working rule or rule not working
			delete rules.author;
			if ( meta.author ) {
				// Try to find if a DOM element matches the authors name
				$( 'body' ).find( '*' ).each( function() {
					if ( $( this ).text() === meta.author ) {
						const sel = fullSelector( this );
						if ( $( sel ).length === 1 ) rules.author = sel;
						return false;
					}
				} );
			}
		}
		// Date -----
		//TODO Don't do that if initial meta reading was successful. Use another data- field to the html element.
		//TODO try getting the info in meta first
		if ( rules.pubdate || rules.moddate || meta.pubdate || meta.moddate ) {
			// If no working rule or rule not working
			// Prepare the reference (meta) dates and those obtained by preexisting rules
			const dateRepr = ( d ) => ( d || '' ).replace( /[^0-9]+/g, '.' );
			const r = {};
			for ( const key of [ 'pubdate', 'moddate' ] ) {
				meta[ key ] = dateRepr( meta[ key ] );
				if ( rules[ key ] ) {
					r[ key ] = $( rules[ key ] )[ 0 ];
					r[ key ] = ( r[ key ] ) ? dateRepr( r[ key ].attr( 'datetime' ) ) : '';
				} else {
					r[ key ] = '';
				}
			}
			if ( r.moddate && ! meta.moddate && r.moddate.startsWith( meta.pubdate ) ) r.moddate = meta.moddate;
			// When mismatch between meta and rules
			el = null;
			for ( const key of [ 'pubdate', 'moddate' ] ) {
				if ( ! r[ key ].startsWith( meta[ key ] ) ) {
					// Try to find the date as a datetime attribue
					delete rules[ key ];
					el = el || $( 'body' ).find( '*[datetime]' );
					el.each( function() {
						if ( dateRepr( $( this ).attr( 'datetime' ) ).startsWith( meta[ key ] ) ) {
							rules[ key ] = fullSelector( this ) + '[datetime]';
							el = null;
							return false;
						}
					} );
					if ( el === null ) continue;
				}
			}
		}
		// Content -----
		const splitMatch = ( elements, targetClass ) => [ elements.filter( targetClass ), elements.filter( `:not(${ targetClass })` ) ];
		const tmpClass = `.${ snipid }-TMP`;
		for ( const key of [ 'descr', 'paragraph', 'section' ] ) {
			const classSel = '.' + CLASS[ key ];
			// Check existing rules
			if ( rules[ key ] ) {
				const oldRules = rules[ key ].split( ',' );
				rules[ key ] = [];
				for ( const rule of oldRules ) {
					el = $( rule );
					if ( el.length <= 2 * el.filter( '.' + CLASS[ key ] ).length ) {
						rules[ key ].push( rule );
						el.addClass( CLASS.hidden ); // marks elements as covered by an existing rule
					}
				}
			} else {
				rules[ key ] = [];
			}
			// Identify rules for the remaining elements
			el = $( `.${ CLASS[ key ] }:not(.${ CLASS.hidden })` );
			// As long as elements remains
			while ( el.length ) {
				// Take the first one, get the rule
				const rule = fullSelector( el[ 0 ] );
				// Remove similar elements from the todo list
				el = el.filter( `:not(${ rule })` );
				// Check the validity of the rule:
				// Split true and false positives
				const [ match, miss ] = splitMatch( $( rule ), classSel );
				let todoMiss = miss;
				let missCount = 0;
				let exRules = [];
				while ( todoMiss.length ) {
					// Go through the false negatives, to true to identify exclusion rules
					let exRule, n = 0;
					todoMiss.each( function() {
						// Find the next element with distinct selector (tag elements with same selectors)
						exRule = fullSelector( this );
						if ( exRule === rule ) {
							n--;
							$( this ).addClass( tmpClass );
						} else {
							n *= -1;
							return false;
						}
					} );
					// If found some elements with identical selector, count as miss
					if ( n ) {
						let sameSel;
						[ sameSel, todoMiss ] = splitMatch( todoMiss, tmpClass );
						sameSel.removeClass( tmpClass );
						if ( n > 1 ) {
							missCount += n;
						} else {
							missCount -= n;
							break; // No more element with distinct selector
						}
					}
					// Test exclusion rule
					[ n, todoMiss ] = splitMatch( todoMiss, exRule );
					if ( miss.filter( exRule ).length > match.filter( exRule ).length ) {
						exRules.push( exRule );
					} else {
						missCount += n.length;
					}
				}
				// If more true positives than false positives, add the rule
				if ( match.length > missCount ) {
					if ( exRules.length ) {
						// First, remove selector redundancy in exclusions (if any)
						const decompose = ( r ) => {
							r = rule.toLowerCase().split( '>' );
							for ( let k = 0; k < r.length; k++ ) {
								r[ k ] = r[ k ].split( /(?=[.#])/ );
								r[ k ].shift();
							}
							return r;
						};
						const r = decompose( rule );
						exRules = exRules.map( ( exRule ) => {
							exRule = decompose( exRule );
							for ( let k = 0; k < r.length; k++ ) {
								for ( const c of r[ k ] ) {
									const i = exRule[ k ].indexOf( c );
									if ( i > -1 ) exRule[ k ].splice( i, 1 );
								}
							}
							exRule = exRule.map( ( x ) => x.join( '' ) ).join( '>' );
						} );
						// Then add rule with exclusion
						rules[ key ].push( `${ rule }:not(${ exRules.join( ',' ) })` );
					} else {
						// Just add rule if no exclusion
						rules[ key ].push( rule );
					}
				}
			}
			rules[ key ] = rules[ key ].join( ',' );
		}
		// Store
		for ( const key in rules ) {
			if ( ! rules[ key ] ) delete rules[ key ];
		}
		chrome.storage.sync.set( { [ hostname ]: rules } );
	};
	
	/*********************\
	|  ARTICLE REWRITING  |
	\*********************/

	/** Rewrite the article, based on previously obtained/selected informations
	 */
	const rewriteArticle = () => {
		// Learning
		updateLearned();
		// Rewriting
		const article = $( '<div id="article"></div>' );
		// If any section element, create the tag converstion function sec
		let sec = $( '.' + CLASS.section );
		if ( sec.length ) {
			// Identify the used tags for sections
			let ordr = [ 'H1', 'H2', 'H3', 'H4', 'H5', 'H6', 'DIV', 'P' ];
			const lvls = [];
			sec.each( function() {
				const t = this.tagName;
				if ( ! lvls.includes( t ) ) {
					lvls.push( t );
				}
			} );
			// Sort them by importancy
			lvls.sort( ( x, y ) => ( ( ( ordr.indexOf( x ) + 20 ) % 20 ) - ( ( ordr.indexOf( y ) + 20 ) % 20 ) ) );
			// Extract a conversion function
			ordr = [ 'H2', 'H3', 'H4', 'H5', 'H6' ];
			sec = ( s ) => ordr[ Math.min( lvls.indexOf( s ), ordr.length - 1 ) ];
		}
		// How paragraph numbers are created and kept track of
		let parCount = 0;
		const parNum = () => {
			const e = $( document.createElement( 'div' ) );
			e.addClass( 'parnum' );
			e.text( ( ++parCount ) + '.' );
			return e;
		};
		// How to convert and transfer a set of elements
		const textBlock = ( el ) => {
			// To track ongoing lists, ongoing blockquotes, where elements are added (article or blockquote)
			let newLi, origLi, origBQ, target;
			// Loop through element (in order)
			el.each( function() {
				// Tag and text content (html formatting)
				const $this = $( this );
				const tag = this.tagName;
				const empty = ! $this.text().trim();
				let descr = $this.hasClass( CLASS.descr );
				let pnum = ! descr;
				// If the element is a section title
				if ( $this.hasClass( CLASS.section ) ) {
					if ( empty ) {
						// If empty, keep only if <hr>
						if ( tag === 'HR' ) {
							article.append( '<hr>' );
						}
					} else {
						// Else, create the right section title element and feel with formatted text
						article.append( textify( $this, sec( tag ) ) );
					}
					origLi = null;
					origBQ = null;
					return;
				}
				// Go up to blockquote, if any
				let bq = this;
				while ( bq ) {
					if ( bq.tagName === 'BLOCKQUOTE' ) break;
					bq = bq.parentElement;
				}
				// If in a blockquote
				if ( bq ) {
					// If new quote, create it, make it the new target for appending
					if ( bq !== origBQ ) {
						target = $( document.createElement( 'blockquote' ) );
						if ( descr ) target.addClass( 'descr' );
						if ( pnum ) target.prepend( parNum() );
						article.append( target );
						origBQ = bq;
					}
					descr = false;
					pnum = false;
				} else {
					// If not in a blockquote, the target of appending is the article
					target = article;
				}
				// If no text, keep only if <hr>
				if ( empty ) {
					if ( tag === 'HR' ) article.append( '<hr>' );
					return;
				}
				// If in a list
				let e;
				if ( tag === 'LI' ) {
					// If new list, create it
					if ( this.parentElement !== origLi ) {
						origLi = this.parentElement;
						newLi = $( document.createElement( origLi.tagName ) );
						target.append( newLi );
					}
					// Add element to list
					e = 'li';
				} else {
					// Else, paragraph
					e = 'p';
				}
				e = textify( $this, e );
				if ( descr ) e.addClass( 'descr' );
				if ( pnum ) e.prepend( parNum() );
				article.append( e );
			} );
		};
		// Get metadata
		const allmeta = getMeta();
		// Publisher
		if ( allmeta.publisher ) {
			const e = $( document.createElement( 'div' ) );
			e.addClass( 'publisher' );
			e.text( allmeta.publisher );
			article.append( e );
		}
		// Title
		$( '.' + CLASS.title ).each( function() {
			article.append( textify( $( this ), 'h1' ) );
		} );
		// Date
		if ( allmeta.pubdate || allmeta.moddate ) {
			const e = $( document.createElement( 'div' ) );
			e.addClass( 'date' );
			if ( allmeta.pubdate ) {
				e.text( dateStr( allmeta.pubdate ) );
				if ( allmeta.moddate ) {
					const d = $( document.createElement( 'small' ) );
					d.text( `(mod. ${ allmeta.moddate })` );
					e.append( d );
				}
			} else {
				e.text( dateStr( allmeta.moddate ) );
			}
			article.append( e );
		}
		// Author
		if ( allmeta.author ) {
			const e = $( document.createElement( 'div' ) );
			e.addClass( 'author' );
			e.text( allmeta.author );
			article.append( e );
		}
		// Description
		article.append( '<br><br>' );
		textBlock( $( '.' + CLASS.descr ) );
		// Content
		textBlock( $( '.' + CLASS.section + ',.' + CLASS.paragraph ) );
		// Clear page
		$head.empty();
		$body.empty();
		// Refill page
		$head.append( '\
			<style>\
				#article {\
					margin: 3.3em 3.3em;\
					font-family: Georgia;\
				}\
				.publisher {\
					font-style: italic;\
					font-weight: bold;\
				}\
				.descr {\
					font-weight: bold;\
					font-size: 1.2em;\
				}\
				.date, .author {\
					font-family: "Century Gothic";\
					color: #888;\
				}\
				p {\
					text-align: justify;\
				}\
				a {\
					color: #7272ff;\
					text-decoration: none;\
				}\
				h1 {\
					font-size: 2.3em;\
					margin: 0 0 0.3em 0;\
				}\
				h2 {\
					font-size: 1.4em;\
				}\
				.parnum {\
					position: absolute;\
					left: 1rem;\
					text-align: right;\
					font-family: "Century Gothic";\
					color: #888;\
					font-size: .9rem;\
				}\
			</style>\
		' );
		$body.append( article );
	};

	/**********************************\
	|  PAGE READING / DATA COLLECTION  |
	\**********************************/

	/** Clean what was added to the page by the plugin */
	const cleanPage = () => {
		// Clean custom styling
		$( `style[data-${ snipid }]` ).remove();
		$( `meta[data-${ snipid }-metastorage]` ).remove();
		// Clean class attributions
		for ( const key in CLASS ) {
			$( '.' + CLASS[ key ] ).removeClass( CLASS[ key ] );
		}
		// Clean event listeners
		for ( const h of [ 'mousemove', 'keydown', 'keyup', 'mousedown', 'contextmenu' ] ) {
			$body.off( h + '.' + snipid );
		}
		// Remove javascript objects attached to body
		delete $body[ 0 ][ `${ snipid }-clean` ];
		delete $body[ 0 ][ `${ snipid }-initrules` ];
	};

	/** Initialize the content detection:
	 *  Try to identify the publisher, title, author, date, description and content,
	 *  and add the required listeners.
	 */
	const initialize = () => {

		const allmeta = {};

		// ----------------------
		//   ROLE MANIPULATIONS
		// ----------------------

		/** Registers a string or $DOM element as being the title
		 *  @param {($DOM|string)} title - Title
		 */
		const setTitle = ( title ) => {
			title = setTitleEl( title );
			if ( title === null ) delete allmeta.title;
			else allmeta.title = title;
			setMeta( { title: title } );
		};

		/** Removes role from target elements.
		 *  @param {$DOM} el - Target elements
		 *  @param {string} selClass - Role to remove, as a class name
		 */
		const removeRole = ( el, selClass ) => {
			el.find( '*' ).removeClass( selClass );
			el.removeClass( selClass );
			el.parents().removeClass( selClass );
		};

		/** Attributes a role to target elements.
		 *  @param {$DOM} el - Target elements
		 *  @param {string} selClass - Role to attribute, as a class name
		 */
		const attributeRole = ( el, selClass ) => {
			// Nothing if empty
			if ( ! el.length ) return;
			// One by one
			if ( el.length > 1 ) {
				el.each( function() {
					attributeRole( $( this ), selClass );
				} );
				return;
			}
			// Exit if attribution in ascendance
			if ( el.hasClass( selClass ) || el.parents().hasClass( selClass ) ) return;
			// Clean previous attributions
			for ( const c in CLASS ) {
				removeRole( el, CLASS[ c ] );
			}
			// Ascend container if relevant
			// i.e. as well as the current element is text formatting and going up does not add text
			let e = null;
			const t = el.text().trim();
			while ( formatTags.includes( el[ 0 ].tagName ) ) {
				e = el;
				el = el.parent();
				if ( el.text().trim() !== t ) {
					el = e;
					break;
				}
			}
			if ( e ) {
				if ( t ) {
					el.addClass( selClass );
				}
				return;
			}
			// Else descend through containers
			const attributeDown = ( x ) => {
				let children = [];
				x.contents().each( function() {
					if ( ( this.nodeType === 3 ) || ( formatTags.includes( this.tagName ) ) ) {
						if ( $( this ).text().trim() || this.tagName === 'BR' ) {
							// There is a text child -> stop digging
							children = null;
							return false;
						}
					} else {
						children.push( $( this ) );
					}
				} );
				if ( children ) {
					for ( const c of children ) {
						attributeDown( c );
					}
				} else if ( x.text().trim() || x[ 0 ].tagName === 'HR' ) {
					x.addClass( selClass );
				}
			};
			attributeDown( el );
		};

		/** Updates the roles of a set of targets as an overall toggle:
		 *  If all targetted text content has the target role, removes it for al elements.
		 *  Else, attributes the role to all.
		 *  @param {$DOM} el - Target elements
		 *  @param {string} selClass - Role to toggle, as a class name
		 */
		const toggleSelection = ( el, selClass ) => {
			const allSelected = ( e ) => {
				if ( e.length === 0 ) {
					return true;
				}
				if ( e.length === 1 ) {
					if ( e[ 0 ].nodeType === 3 ) {
						return ! e;
					}
					if ( e.hasClass( selClass ) ) {
						return true;
					}
					return allSelected( e.contents() );
				}
				let selected = true;
				e.each( function() {
					if ( ! allSelected( $( this ) ) ) {
						selected = false;
						return false;
					}
				} );
				return selected;
			};
			if ( allSelected( el ) ) {
				removeRole( el, selClass );
			} else {
				attributeRole( el, selClass );
			}
		};

		// --------------------------
		//   MAKE ROLE VISIBLE: CSS
		// --------------------------

		// Inject CSS for the roles
		$head.append(
			`<style data-${ snipid }>` +
				`.${ CLASS.title }{background-color:#ffa5c2}` +
				`.${ CLASS.descr }{background-color:#a5f3ff}` +
				`.${ CLASS.paragraph }{background-color:#fffb85}` +
				`.${ CLASS.section }{background-color:#ffbd85}` +
				`.${ CLASS.mouseover }{color:#ff3333 !important}` +
				`.${ CLASS.hidden }{display:none}` +
			`</style>`
		);
		
		// ----------------
		//   HTML READING
		// ----------------

		const textContainerDescriptor = ( el, childSel ) => {
			let sel = ( el.className || '' ).trim();
			if ( sel ) {
				sel = sel.split( /\s+/ ).sort();
				sel.unshift( el.tagName );
				sel = sel.join( '.' );
			} else {
				sel = el.tagName;
			}
			if ( childSel ) {
				sel += '>' + childSel;
			}
			const par = el.parentNode;
			if ( ( par.childElementCount < 2 ) && ( $( par ).text() === $( el ).text() ) && par !== $body ) {
				return textContainerDescriptor( par, sel );
			}
			return [ par, sel ];
		};
		
		// --------------------
		//   READING METADATA
		// --------------------
		
		const storedRules = $body[ 0 ][ `${ snipid }-initrules` ] || {};
		console.log( storedRules );

		// From what had been learnt
		const found = {};
		for ( const key of [ 'publisher', 'lang' ] ) {
			if ( storedRules[ key ] ) allmeta[ key ] = storedRules[ key ];
		}
		if ( storedRules.title ) {
			setTitle( $( storedRules.title ) );
			found.title = true;
		}
		for ( const key of [ 'author', 'pubdate', 'moddate' ] ) {
			if ( storedRules[ key ] ) {
				if ( typeof storedRules[ key ] === 'string' ) {
					const el = $( storedRules[ key ] );
					if ( el.length ) {
						if ( key === 'author' ) {
							allmeta.author = el.text();
						} else {
							allmeta[ key ] = metaDateRestring( el.attr( 'datetime' ) );
						}
					}
				} else {
					metaMap[ key ].unshift( storedRules[ key ] );
				}
			}
		}
		for ( const key of [ 'descr', 'paragraph', 'section' ] ) {
			if ( storedRules[ key ] ) {
				const el = $( storedRules[ key ] );
				if ( el.length ) {
					found[ key ] = true;
					attributeRole( el, CLASS[ key ] );
				}
			}
		}

		// Read the metadata
		for ( const key in metaMap ) {
			if ( allmeta[ key ] || found[ key ] ) continue;
			for ( const fields of metaMap[ key ] ) {
				const r = {};
				let x = null;
				for ( const f of fields ) {
					if ( f.includes( '=' ) ) {
						x = $( 'meta[' + f + ']' ).attr( 'content' );
					} else {
						x = $( f ).text();
					}
					if ( x ) {
						switch ( key ) {
							case 'pubdate':
								// For publication date: keep the earliest value
								x = metaDateRestring( x );
								r.date = ( r.date ) ? earliest( r.date, x ) : x;
								break;
							case 'moddate':
								// For publication date: keep the latest value
								x = metaDateRestring( x );
								r.date = ( r.date ) ? latest( r.date, x ) : x;
								break;
							default:
								// For other fields: keep the most common
								if ( r[ x ] ) {
									r[ x ]++;
								} else {
									r[ x ] = 1;
								}
						}
					}
				}
				if ( key.endsWith( 'date' ) ) {
					if ( r.date ) {
						allmeta[ key ] = r.date;
						break;
					}
				} else {
					let y = null;
					let n = 0;
					for ( const z in r ) {
						if ( r[ z ] > n ) {
							n = r[ z ];
							y = z;
						}
					}
					if ( y ) {
						allmeta[ key ] = y;
						break;
					}
				}
			}
		}

		// Detect language
		if ( ! allmeta.lang ) {
			if ( document.domain.endsWith( '.fr' ) ) {
				allmeta.lang = 'fr';
			} else {
				for ( const x of [ $( 'html' ).attr( 'lang' ), $( 'meta[property="og:locale"]' ).attr( 'content' ) ] ) {
					if ( x && x.toLowerCase().startsWith( 'fr' ) ) {
						allmeta.lang = 'fr';
						break;
					}
				}
			}
		}

		// --------------------
		//   PAGE EXPLORATION
		// --------------------

		// Find the title on the page
		if ( ! found.title ) {
			let set = false;
			if ( allmeta.title ) {
				// If a title has been found in the metadata, we look for a matching element
				const ref = allmeta.title.toLowerCase();
				for ( const titleEl of [ 'h1', 'h2', 'h3', 'div' ] ) {
					$( titleEl ).each( function() {
						const t = $( this ).text();
						if ( t && ref.includes( t.toLowerCase() ) ) {
							setTitle( $( this ) );
							set = true;
							return false;
						}
					} );
					if ( set ) break;
				}
			}
			if ( ! set ) {
				// If no metadata title, or no match in page, we look for a unique <h1> element
				const t = $( 'h1' );
				if ( t.length === 1 ) setTitle( t );
			}
		}

		// Find the description in the page
		if ( storedRules.descr ) {
			$( storedRules.descr ).attributeRole( CLASS.descr );
		} else if ( allmeta.desrc ) {
			const ref = allmeta.desrc.toLowerCase();
			for ( const descrEl of [ 'h2', 'h3', 'h4', 'p', 'div' ] ) {
				$( descrEl ).each( function() {
					const t = $( this ).text();
					if ( t && ref.includes( t.substr( 0, 60 ).toLowerCase() ) ) {
						attributeRole( $( this ), CLASS.descr );
						return false;
					}
				} );
			}
			delete allmeta.desrc;
		}

		// Save identified metadata
		setMeta( allmeta );

		// Find the article
		if ( storedRules.paragraph ) {
			attributeRole( $( storedRules.paragraph ), CLASS.paragraph );
			if ( storedRules.section ) attributeRole( $( storedRules.section ), CLASS.section );
		} else {
			//TODO automatic section description
			const selDict = {};
			let topSel = [ 0, null, '' ];
			let par, sel;
			$( 'p' ).each( function() {
				[ par, sel ] = textContainerDescriptor( this );
				if ( selDict[ sel ] ) {
					let unfound = true;
					for ( const t of selDict[ sel ] ) {
						if ( t[ 0 ] === par ) {
							if ( ++t[ 1 ] > topSel[ 0 ] ) {
								topSel = [ t[ 1 ], par, sel ];
							}
							unfound = false;
							break;
						}
					}
					if ( unfound ) {
						selDict[ sel ].push( [ par, 1 ] );
						if ( ! topSel[ 0 ] ) {
							topSel = [ 1, par, sel ];
						}
					}
				} else {
					selDict[ sel ] = [ [ par, 1 ] ];
					if ( ! topSel[ 0 ] ) {
						topSel = [ 1, par, sel ];
					}
				}
			} );
			$( topSel[ 1 ] ).find( topSel[ 2 ] ).addClass( CLASS.paragraph );
		}

		// ------------------------------------
		//   USER-BASED REFINEMENT: LISTENERS
		// ------------------------------------

		/* eventListeningStatus is used to keep track of the current state:
		 *     .cursorTarget:
		 *           Stores the target of the cursor.
		 *           Updated when mouse moved.
		 *     .selectedTargets:
		 *           Stores the derived targets depending on pressed keys.
		 *           Updated when mouse moved, or when derivation rule are changed (based on pressed keys).
		 *     .readTargets:
		 *           Function to update the .selectedTargets based on .cursorTarget.
		 *     .ignore:
		 *           Last key pressed, that should be ignore if pressed again (repeated keydown event).
		 */
		const eventListeningStatus = { readTargets: () => { } };
		
		/** Redefines the mouse target derivation rules (eventListeningStatus.readTargets)
		*   based on the currently pressed keys.
	    *   @param {object} evt - Listener event
		*/
		const keyChangeUpdateListeners = ( evt ) => {
			if ( [ 'ControlLeft', 'ShiftLeft' ].includes( evt.code ) ) {
				if ( evt.ctrlKey ) {
					eventListeningStatus.readTargets =
						( evt.shiftKey ) ?
							( () => {
								$( '.' + CLASS.mouseover ).removeClass( CLASS.mouseover );
								const [ par, sel ] = textContainerDescriptor( eventListeningStatus.cursorTarget );
								eventListeningStatus.selectedTargets = $( par ).find( sel );
								eventListeningStatus.selectedTargets.addClass( CLASS.mouseover );
							} ) :
							( () => {
								$( '.' + CLASS.mouseover ).removeClass( CLASS.mouseover );
								eventListeningStatus.selectedTargets = $( eventListeningStatus.cursorTarget );
								eventListeningStatus.selectedTargets.addClass( CLASS.mouseover );
							} );
					eventListeningStatus.readTargets();
				} else {
					$( '.' + CLASS.mouseover ).removeClass( CLASS.mouseover );
					eventListeningStatus.readTargets = () => { };
					eventListeningStatus.selectedTargets = null;
				}
			} else if ( evt.type === 'keydown' ) {
				switch ( evt.code ) {
					case 'Enter':
						rewriteArticle();
						break;
					case 'Escape':
						cleanPage();
						break;
					case 'Space':
						if ( eventListeningStatus.selectedTargets ) {
							if ( evt.altKey ) toggleSelection( eventListeningStatus.selectedTargets, CLASS.descr );
							else setTitle( eventListeningStatus.selectedTargets );
						}
						break;
				}
			}
		};

		// Listener: Mouse moved: Update target
		$body.on( 'mousemove.' + snipid, ( evt ) => {
			eventListeningStatus.cursorTarget = evt.target;
			eventListeningStatus.readTargets();
		} );

		// Listener: Key pressed or released: Update targets derivation rule
		$body.on( 'keydown.' + snipid, ( evt ) => {
			if ( evt.code === eventListeningStatus.ignore ) {
				return;
			}
			eventListeningStatus.ignore = evt.code;
			keyChangeUpdateListeners( evt );
		} );
		$body.on( 'keyup.' + snipid, ( evt ) => {
			if ( evt.code === eventListeningStatus.ignore ) {
				eventListeningStatus.ignore = null;
			}
			keyChangeUpdateListeners( evt );
		} );

		// Listener: Mouse click: Update targets
		$body.on( 'mousedown.' + snipid, ( evt ) => {
			if ( eventListeningStatus.selectedTargets ) {
				switch ( evt.which ) {
					case 1:
						// Left click: Add to or remove from content. + Shift for section title
						toggleSelection( eventListeningStatus.selectedTargets, ( evt.altKey ) ? CLASS.section : CLASS.paragraph );
						break;
					case 2:
						// Middle click: Title (select/switch)
						setTitle( eventListeningStatus.selectedTargets );
						break;
					case 3:
						// Right click: Add to or remove from abstract.
						toggleSelection( eventListeningStatus.selectedTargets, CLASS.descr );
						break;
				}
				return false; // Deactivate middle click
			}
		} );
		$body.on( 'contextmenu.' + snipid, () => {
			if ( eventListeningStatus.selectedTargets ) return false; // Deactivate right click menu opening
		} );

	};

	// Listener: Requets from the popup
	chrome.runtime.onMessage.addListener(
		function( request, sender, sendResponse ) {
			if ( request.bdreformat_MSG ) {
				switch ( request.bdreformat_MSG ) {
					case 'clean':
						// Clean what the plugin did
						cleanPage();
						return;
					case 'rewrite':
						// Rewrite article (requires prior initialization)
						if ( $head.children( `style[data-${ snipid }]` ) ) {
							rewriteArticle();
							return;
						}
						break;
					case 'setmeta':
						// Set metadata
						delete request.bdreformat_MSG;
						if ( request.title || request.title === '' ) setTitleEl( request.title );
						setMeta( request );
						return;
					case 'reset':
						cleanPage();
						break;
					case 'fullreset':
						delete $body[ 0 ][ `${ snipid }-initrules` ];
						chrome.storage.sync.remove( hostname );
						cleanPage();
						break;
				}
				// Initialize if needed
				if ( ! $head.children( `style[data-${ snipid }]` )[ 0 ] ) initialize();
				// Send back meta data
				const meta = getMeta();
				if ( $body[ 0 ][ `${ snipid }-initrules` ] ) meta.known = true;
				sendResponse( meta );
			}
		}
	);

} )();

//TODO Learning absorbs class names and id that could include post-specific ids, making it unfunctional.
